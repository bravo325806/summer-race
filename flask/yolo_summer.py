from darkflow.net.build import TFNet
import numpy as np
import cv2
import requests
import os

class YOLO:
    model_file = 'tiny-yolo-summer.cfg'
    count = 0
    def __init__(self):
        self.options = {"model": self.model_file, "load": 10000, "threshold": 0.25}
        self.tfnet = TFNet(self.options)
        self.colors = [tuple(255 * np.random.rand(3)) for i in range(10)]
        self.camera = cv2.VideoCapture(1)

    def __del__(self):
        self.camera.release()
    
    def detect(self):
        ret, frame = self.camera.read()
        results = self.tfnet.return_predict(frame)
        if len(results):
            cv2.imwrite(str(self.count)+'.jpg', frame)
            self.upload(results)
            self.count += 1
        for color, result in zip(self.colors, results):
            label = result['label']
            conf = result['confidence']
            top_left_position = (result['topleft']['x'], result['topleft']['y'])
            bottom_right_position = (result['bottomright']['x'], result['bottomright']['y'])
            note = '{}: {:.0f}%'.format(label, conf * 100)
            frame = cv2.rectangle(frame, top_left_position, bottom_right_position, color, 5)
            frame = cv2.putText(frame, note, top_left_position, cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 2)
        ret, jpeg = cv2.imencode('.jpg', frame)
        return jpeg.tobytes()
    
    def upload(self, results):
        files = {'file': open(str(self.count)+'.jpg', 'rb')}
        requests.post(url='http://211.20.7.115:31568/upload', files=files)
        os.remove(str(self.count)+'.jpg')
