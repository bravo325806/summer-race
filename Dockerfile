FROM tensorflow/tensorflow:latest-gpu-py3

WORKDIR /

RUN apt update && apt install -y git
RUN pip install opencv-python==3.2.0.8 flask
RUN git clone https://github.com/bravo325806/summer_race.git

WORKDIR /summer_race

COPY YOLO_tiny.ckpt /summer_race/weights/

CMD python3 main.py